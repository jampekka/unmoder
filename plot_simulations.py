# Copyleft 2019 Jami Pekkanen <pscjjop@leeds.ac.uk>
# Released under AGPLv3, see LICENCE

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
from scipy.interpolate import interp1d
import itertools

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Roboto"
plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["font.size"] = 13
plt.rcParams['svg.fonttype'] = 'none'

COLOR = '#003248ff'
plt.rcParams['text.color'] = COLOR
plt.rcParams['axes.labelcolor'] = COLOR
plt.rcParams['xtick.color'] = COLOR
plt.rcParams['ytick.color'] = COLOR


from model_steering import takeover_circle_pursuiter, circle_pursuit_target

cmap = plt.get_cmap("cool")

def get_dx(t, gamma, alpha0, v):
    return (v*np.select([np.not_equal(gamma, 0),True], [-np.cos(alpha0 + gamma*t)/gamma,t*np.sin(alpha0)], default=np.nan) + np.select([np.not_equal(gamma, 0),True], [v*np.cos(alpha0)/gamma,0], default=np.nan))

def get_dy(t, gamma, alpha0, v):
    return (v*np.select([np.not_equal(gamma, 0),True], [np.sin(alpha0 + gamma*t)/gamma,t*np.cos(alpha0)], default=np.nan) + np.select([np.not_equal(gamma, 0),True], [-v*np.sin(alpha0)/gamma,0], default=np.nan))

def integrate_yawrates(ts, speed, yawrates, x0=0, y0=0, h0=0):
    dt = np.ediff1d(ts, to_begin=0)
    heading = h0 + np.cumsum(yawrates*dt)
    xs = []
    #dx = speed*(np.cos(heading) - np.cos(yawrates*(heading/yawrates + dt)))/(yawrates)
    #dx[~np.isfinite(dx)] = 0.0
    #dy = speed*(-np.sin(heading) + np.sin(yawrates*(heading/yawrates + dt)))/(yawrates)
    #dy[~np.isfinite(dy)] = speed
    dx = np.sin(heading)*speed*dt
    #dx = get_dx(dt, yawrates, heading, speed)
    dy = np.cos(heading)*speed*dt
    #dy = get_dy(dt, yawrates, heading, speed)
    #dy = np.sinc(yawrates*dt/np.pi)*speed*dt
    x = x0 + np.cumsum(dx)
    y = y0 + np.cumsum(dy)
    return x, y, heading

def ttlc_from_offset(b, w, r, v):
    return np.sign(b)*np.sqrt(w*(2*r + np.sign(b)*w)/(np.abs(b)*r*v))

speed = 8
duration = 30
radius = 80
origin = np.array((radius, 0))
dt = 1/100.0
ts = np.arange(0, duration, dt)
onset = 1.0
ts -= onset

def lane_departure(x, y):
    return np.linalg.norm(np.array((x, y)).T - origin, axis=1) - radius

in_x, in_y, _ = integrate_yawrates(ts, speed, speed/(radius - 1.5), x0=1.5)
out_x, out_y, _ = integrate_yawrates(ts, speed, speed/(radius + 1.5), x0=-1.5)

c_x, c_y, _ = integrate_yawrates(ts, speed, speed/radius)

f = lambda t: np.exp(-1/t)*(t > 0)
_smooth_step = lambda t: f(t)/(f(t) + f(1 - t))
eps = np.finfo(float).eps
def smooth_step(t):
    out = _smooth_step(t)
    out[t <= 0+eps] = 0.0
    out[t >= 1-eps] = 1.0
    return out
transition_duration = .5

ref_yawrate = np.repeat(speed/radius, len(ts))
ref_yawrate -= smooth_step(ts/transition_duration)*ref_yawrate
ref_x, ref_y, ref_h = integrate_yawrates(ts, speed, ref_yawrate)

params = pd.read_csv('steering_fits.csv')
params['active_dur'] = params['duration'] - params['takeover_time']
params = params.query('active_dur > 3')

params['reaction_time'] =  params['takeover_time'] - params['onset_time']
params = params[params.reaction_time > 0]
params = params[params.sab.abs() > 1.0]

params['lpreview'] = np.log(params.preview)
params['ltime_constant'] = np.log(params.time_constant)
params['lthreshold'] = np.log(params.threshold)
def get_outliers(d):
    h, m, l = d.quantile([0.25, 0.5, 0.75])
    return np.abs((d - m)) > 2*(l - h)
outliers = get_outliers(params.lpreview) | get_outliers(params.ltime_constant) | get_outliers(params.threshold)
params = params[~outliers]


params['ttlc'] = ttlc_from_offset(np.radians(params.sab), 1.5, speed, duration)

part_params = params.groupby(['ppid', 'cogload']).mean()
part_params[['preview', 'time_constant', 'threshold']] = np.exp(part_params[['lpreview', 'ltime_constant', 'lthreshold']])

cond_params = part_params.groupby("cogload").mean()

loaded = cond_params.loc['Middle']
unloaded = cond_params.loc['None']


def takeover_vs_yawrate():
    from reconstruct_trajectory import get_untouched_trajectories, trialcols, get_track
    data = get_untouched_trajectories()
    empirical = []
    for d in data.values():
        if d["sab"] != -5.72957795: continue
        if d["cogload"] != 'None': continue
        rt = d["takeover_time"] - d["onset_time"]
        if rt < 0: continue
        traj = d['otraj'].query('not autoflag')
        maxyr = traj['yawrate_seconds'].abs().max()
        empirical.append((rt, maxyr))
    
    empirical = np.array(empirical)
    empirical = empirical[np.isfinite(empirical[:,0]) & (empirical[:,0] < 5)]
    plt.figure("deps")
    plt.plot(ts, lane_departure(ref_x, ref_y), color='black', linestyle='dashed')


    
    interps = []
    for p, pp in part_params.groupby('ppid'):
        takeovers = []
        maxyrs = []
        pp = pp.loc[(p, 'None')]
        devrng = np.linspace(pp.threshold/3, pp.threshold*3, 10)
        for dev in devrng:
            yawrate, error = takeover_circle_pursuiter(
                pp.preview, pp.time_constant, pp.time_constant, dev,
                origin, radius, speed,
                (ts, ref_x, ref_y, np.degrees(ref_h), np.degrees(ref_yawrate))
                )
            yawrate = np.radians(yawrate)
            x, y, h = integrate_yawrates(ts, speed, yawrate)
            
            takeover = np.flatnonzero(np.abs(yawrate - ref_yawrate) > 1e-6)[0]
            departure = lane_departure(x, y)
            color = cmap((dev - devrng[0])/devrng[-1])

            takeover_time = ts[takeover]
            maxyr = np.max(np.abs(yawrate))
            takeovers.append(takeover_time)
            maxyrs.append(maxyr)
            plt.figure("deps")
            plt.plot(ts[takeover:], departure[takeover:], zorder=1, color=color, lw=2)
            plt.plot(ts[takeover], departure[takeover], 'o', zorder=3, color=color)
        interps.append(interp1d(takeovers, maxyrs)) 
        plt.figure("accels")
        #plt.plot(takeovers, np.degrees(maxyrs))
    
    rng = np.max([i.x[0] for i in interps]), np.min([i.x[-1] for i in interps])
    rng = np.linspace(*rng, 100)
    interps = np.array([i(rng) for i in interps])
    plt.plot(rng, np.degrees(np.mean(interps, axis=0)))
    m = np.mean(interps, axis=0)
    s = np.std(interps, axis=0)*1.96
    plt.fill_between(rng, np.degrees(m - s), np.degrees(m + s), alpha=0.1)

    plt.figure("deps")
    #plt.plot(ts, lane_departure(in_x, in_y), color='black', alpha=0.3)
    #plt.plot(ts, lane_departure(out_x, out_y), color='black', alpha=0.3)
    plt.axhline(-1.5, color='black', alpha=0.3, zorder=0)
    plt.axhline(0.0, color='black', linestyle='dashed', alpha=0.3, zorder=2)
    plt.axhline(1.5, color='black', alpha=0.3, zorder=0)

    plt.xlabel("Time (seconds)")
    plt.ylabel("Lane departure (meters)")
    plt.xlim(-1, 10)
    plt.ylim(-0.5, 3.5)
    

    plt.figure("accels")
    plt.plot(empirical[:,0], empirical[:,1], '.')
    
    plt.figure("takeovers")
    plt.hist(empirical[:,0])

    plt.show()

def loaded_vs_unloaded():
    def plot_params(param, color, label, takeover_at=np.inf, alpha=1.0):
        if not np.isfinite(takeover_at):
            threshold = param.threshold
        else:
            threshold = np.inf
        yawrate, error = takeover_circle_pursuiter(
            param.preview, param.time_constant, param.time_constant, threshold,
            origin, radius, speed,
            (ts, ref_x, ref_y, np.degrees(ref_h), np.degrees(ref_yawrate)),
            force_takeover_at=takeover_at
            )
        yawrate = np.radians(yawrate)
        x, y, h = integrate_yawrates(ts, speed, yawrate)

        takeover = np.flatnonzero(np.abs(yawrate - ref_yawrate) > 1e-6)[0] - 1
        departure = lane_departure(x, y)
        plt.figure("departure")
        plt.plot(ts[takeover:], departure[takeover:], zorder=1, color=color, lw=2, label=label, alpha=alpha)
        plt.plot(ts[takeover], departure[takeover], 'o', zorder=3, color=color, alpha=alpha)

        plt.figure("yawrate")
        plt.plot(ts[takeover:], np.degrees(yawrate[takeover:]), zorder=1, color=color, lw=2, label=label, alpha=alpha)
        plt.plot(ts[takeover], np.degrees(yawrate[takeover]), 'o', zorder=3, color=color, alpha=alpha)

    plot_params(unloaded, cmap(0.0), label="No load", takeover_at=np.inf)
    plot_params(loaded, cmap(1.0), label="Cognitive load", takeover_at=np.inf)

    plt.figure("departure")
    plt.axhline(-1.5, color='black', alpha=0.3, zorder=0)
    plt.axhline(0.0, color='black', linestyle='dashed', alpha=0.3, zorder=2)
    plt.axhline(1.5, color='black', alpha=0.3, zorder=0)
    plt.plot(ts, lane_departure(ref_x, ref_y), color='black', linestyle='dashed')
    plt.xlim(-1, 10)
    plt.ylim(-0.5, 3.5)

    plt.figure("yawrate")
    plt.plot(ts, np.degrees(ref_yawrate), color='black', linestyle='dashed')
    plt.xlim(-1, 10)
    plt.xlabel("Time since onset (seconds)")
    plt.ylabel("Yaw rate (degrees/second)")
    plt.show()

from matplotlib.collections import LineCollection
from matplotlib.patches import Polygon
def fancyline(x, y, *args, **kwargs):
    segs = [[(x[i], y[i]), (x[i+1], y[i+1])] for i in range(len(x)-1)]
    segs = LineCollection(segs, *args, **kwargs)
    return plt.gca().add_collection(segs)

def shift_curve(x, y, w):
    dx = np.gradient(x)
    dy = np.gradient(y)
    angle = np.arctan(dx, dy) + np.pi/2
    dx = np.sin(angle)*w
    dy = np.cos(angle)*w
    x = x + dx
    y = y + dy
    return x, y


def line_to_poly(x, y, w):
    #dx = np.gradient(x)
    #dy = np.gradient(y)
    #angle = np.arctan(dx, dy) + np.pi/2
    #dx = np.sin(angle)*(w/2)
    #dy = np.cos(angle)*(w/2)

    left_x, left_y = shift_curve(x, y, -w/2)
    right_x, right_y = shift_curve(x, y, w/2)

    x = np.concatenate((left_x, right_x[::-1]))
    y = np.concatenate((left_y, right_y[::-1]))

    return x, y


def plot_projected():
    h = 0
    
    def project(x, z, y=-2.0, pitch=np.radians(-3), x0=-2):
        x = x - x0
        y_ = np.cos(pitch)*y - z*np.sin(pitch)
        z_ = np.sin(pitch)*y + z*np.cos(pitch)
        x_ = x
        x_ = x_/z_
        y_ = y_/z_
        invalid = z_ <= 0
        x_[invalid] = np.nan
        y_[invalid] = np.nan
        z_[invalid] = np.nan
        return x_, y_, z_
    
    def perspline(x, y, width=0.08, *args, **kwargs):
        #x, y, z = project(x, y)
        #return fancyline(x, y, 1/z*20, *args, **kwargs)
        x, y = line_to_poly(x, y, width)
        x, y, z = project(x, y)
        settings = {}
        settings.update(kwargs)
        poly = Polygon(np.array((x, y)).T, *args, **settings)
        return plt.gca().add_patch(poly)

    
    pp = unloaded
    devrng = np.linspace(0.0, 2.0, 9)
    
    for dev in devrng:
        yawrate, error = takeover_circle_pursuiter(
            pp.preview, pp.time_constant, pp.time_constant, np.inf,
            origin, radius, speed,
            (ts, ref_x, ref_y, np.degrees(ref_h), np.degrees(ref_yawrate)),
            force_takeover_at=(dev + onset)
            )
        yawrate = np.radians(yawrate)
        x, y, h = integrate_yawrates(ts, speed, yawrate)
        
        takeover = np.flatnonzero(np.abs(yawrate - ref_yawrate) > 1e-6)[0]
        departure = lane_departure(x, y)
        color = cmap((dev - devrng[0])/devrng[-1])

        takeover_time = ts[takeover]
        perspline(*shift_curve(x[takeover:], y[takeover:], -1.0), width=0.05, color=color, zorder=3, alpha=0.8)
        perspline(*shift_curve(x[takeover:], y[takeover:], 1.0), width=0.05, color=color, zorder=3, alpha=0.8)
        #perspline(x, y, width=2.0, color=color, zorder=2, alpha=0.1)

    #_, horizon = project(np.array([0]), np.array([1e6]))
    #plt.axhline(horizon[0], color='black', alpha=0.3)
    perspline(in_x, in_y, color='black', alpha=0.3, lw=0)
    perspline(out_x, out_y, color='black', alpha=0.3, lw=0)
    perspline(c_x, c_y, width=3.0, color='black', alpha=0.05, lw=0)

    plt.gca().set_aspect("equal")
    plt.xlim(0.05, 1.5)
    plt.ylim(-0.35, 0.05)
    plt.show()


def plot_waypoint_demo():
    pp = unloaded
    n = 100

    n = ts.searchsorted(ts[0] + pp.preview) + 1
    nt = ts[:n]

    nref_h = ref_h - np.radians(10.0)
    x0 = 1.0
    nrefx = ref_x + x0
    yawrate, error = takeover_circle_pursuiter(
        pp.preview, pp.time_constant, pp.time_constant, np.inf,
        origin, radius, speed,
        (nt, nrefx, ref_y, np.degrees(nref_h), np.degrees(ref_yawrate)),
        force_takeover_at=0.0
    )
    
    idealyr, error = takeover_circle_pursuiter(
        pp.preview, 1e-9, 1e-9, np.inf,
        origin, radius, speed,
        (nt, nrefx, ref_y, np.degrees(nref_h), np.degrees(ref_yawrate)),
        force_takeover_at=-np.inf
    )
    
    x, y, h = integrate_yawrates(ts[:n], speed, np.radians(yawrate), h0=nref_h[0], x0=x0)
    plt.plot(x, y)

    #idealyr = circle_pursuit_target(pp.preview, pp.time_constant, origin, radius, speed, nrefx[0], nrefx[1])
    
    x, y, h = integrate_yawrates(ts[:n], speed, np.radians(np.repeat(idealyr[0], n)), h0=nref_h[0], x0=x0)
    plt.plot(x, y)

    plt.plot(in_x[:n], in_y[:n], color='black', alpha=0.3)
    plt.plot(out_x[:n], out_y[:n], color='black', alpha=0.3)
    plt.axis("equal")
    plt.show()

def plot_steering_components():
    pp = unloaded

    n = ts.searchsorted(ts[0] + 4.0)
    nt = ts[:n]
    onset_n = nt.searchsorted(0.0) + 1
    #nref_h = ref_h - np.radians(10.0)
    yawrate, error = takeover_circle_pursuiter(
        pp.preview, pp.time_constant, pp.time_constant, pp.threshold,
        origin, radius, speed,
        (nt, ref_x, ref_y, np.degrees(ref_h), np.degrees(ref_yawrate)),
        #force_takeover_at=0.0
    )

    yawrate = np.radians(yawrate)
    error = np.radians(error)
    
    ton = np.flatnonzero(np.abs(yawrate - ref_yawrate[:n]) > 1e-6)[0]
    
    idealyr, _ = takeover_circle_pursuiter(
        pp.preview, 1e-9, 1e-9, pp.threshold,
        origin, radius, speed,
        (nt, ref_x, ref_y, np.degrees(ref_h), ref_yawrate),
        #force_takeover_at=0.0
    )
    
    x, y, h = integrate_yawrates(nt, speed, np.radians(yawrate))
    plt.plot(nt[:ton], np.degrees(yawrate + error)[:ton], '--', color=cmap(0.0), lw=5)
    plt.plot(nt[ton-1], np.degrees(yawrate + error)[ton-1], 'o', color=cmap(0.0), lw=5, zorder=3)
    
    plt.plot(nt, np.degrees(yawrate), color=cmap(0.0), lw=5)
    plt.plot(nt[ton-1], np.degrees(yawrate)[ton-1], 'o', color=cmap(0.0), lw=5, zorder=3)

    plt.plot([nt[ton-1]]*2, [np.degrees(yawrate)[ton-1], np.degrees(yawrate + error)[ton-1]],
            '-', color='black', alpha=0.5)
    
    plt.plot(nt[ton-1], np.degrees(yawrate)[ton-1], 'o', color=cmap(0.0), lw=5, zorder=3)
    
    plt.plot(nt[:onset_n], np.degrees(ref_yawrate[:n][:onset_n]), color='black', lw=3)
    plt.plot(nt[onset_n:ton], np.degrees(ref_yawrate[:n][onset_n:ton]), color=cmap(1.0), lw=5)
    plt.plot(nt[ton:], np.degrees(ref_yawrate[:n][ton:]), '--', color=cmap(1.0), lw=5)
    plt.plot(nt[onset_n], np.degrees(ref_yawrate[:n][onset_n]), 'o', color=cmap(1.0))

    plt.xlabel('Time from failure (seconds)')
    plt.ylabel('Turning rate (degrees/second)')
    plt.xlim(nt[0], nt[-1])

    plt.figure("departure")

    dep = lane_departure(*integrate_yawrates(nt, speed, yawrate)[:2])
    refdep = lane_departure(ref_x, ref_y)
    
    plt.plot(nt[:onset_n], refdep[:n][:onset_n], color='black', lw=5)
    plt.plot(nt[onset_n:ton], refdep[:n][onset_n:ton], color=cmap(1.0), lw=5)
    plt.plot(nt[onset_n], refdep[:n][onset_n], 'o', color=cmap(1.0), lw=5)
    plt.plot(nt[ton:], refdep[:n][ton:], '--', color=cmap(1.0), lw=5)


    plt.plot(nt[ton:], dep[ton:], color=cmap(0.0), lw=5)
    plt.plot(nt[ton-1], dep[ton-1], 'o', color=cmap(0.0), lw=5)
    plt.axhline(1.5, color='black', alpha=0.5)
    plt.xlim(nt[0], nt[-1])
    plt.ylim(-0.5, 2)
    
    plt.xlabel('Time from failure (seconds)')
    plt.ylabel('Lane deviation (meters)')

    plt.show()

def plot_failure_scenarios():
    counts = [(sab, len(d)) for sab, d in params.query("cogload == 'None'").groupby('sab')]
    
    counts = np.sort(counts)
    counts = np.array(counts)
    weights = counts[:,1]
    weights /= np.sum(weights)
    ts = np.arange(-1, 5, dt)
    
    absab = np.abs(counts[:,0])
    rng = np.min(absab), np.max(absab)
    for sab, weight in zip(counts[:,0], weights):
        yawrate = np.repeat(speed/radius, len(ts))
        yawrate[ts > 0] += np.radians(sab)
        x, y, h = integrate_yawrates(ts, speed, yawrate)
        dev = lane_departure(x, y)
        c = (np.abs(sab) - rng[0])/(rng[1] - rng[0])
        plt.plot(x, y, color=cmap(c), lw=2)
        ttlc = ts[np.abs(dev) > 1.5][0]
    
    yawrate = np.repeat(speed/radius, len(ts))
    rx, ry, h = integrate_yawrates(ts, speed, yawrate)
    rx, ry = line_to_poly(rx, ry, 3.0)
    poly = Polygon(np.array((rx, ry)).T, color='black', alpha=0.2, lw=0)
    plt.gca().add_patch(poly)
    plt.axis('equal')
    plt.show()


def sample_trials():
    from reconstruct_trajectory import get_untouched_trajectories, trialcols, get_track
    data = get_untouched_trajectories()
    meancors = []
    trialcors = []
    trials = []
    for s, sds in itertools.groupby(data.items(), lambda kv: kv[0][0]):
        for g, trial in sds:
            od = trial['otraj']
            ud = trial['traj']
            cogload = trial['cogload']
            trialn = trial['trialn']
            if cogload != 'None': continue
            
            try:
                tp = params.query("ppid == @s and trialn == @trialn and cogload == @cogload").iloc[0]
                pp = part_params.query("ppid == @s and cogload == @cogload").iloc[0]
            except IndexError:
                continue
            
            tmin = max(od.ts.iloc[0], ud.ts.iloc[0])
            tmax = min(od.ts.iloc[-1], ud.ts.iloc[-1])
            ud = ud.query("ts >= @tmin and ts <= @tmax")
            od = od.query("ts >= @tmin and ts <= @tmax")

            try:
                onset_i = od.ts.values.searchsorted(trial['onset_time']) - 1
                takeover_i = np.flatnonzero(od.autoflag.values)[-1] - 1
            except IndexError:
                continue

            midline, origin = get_track(od.radius.iloc[0], od.bend.iloc[0] < 0)
            
            yawrate, error = takeover_circle_pursuiter(
                tp.preview, tp.time_constant, tp.time_constant, tp.threshold,
                origin, radius*od.iloc[0].bend, speed,
                ud[['ts', 'world_x', 'world_z', 'world_yaw', 'yawrate_seconds']].values.T
            )
            
            myawrate, error = takeover_circle_pursuiter(
                pp.preview, pp.time_constant, pp.time_constant, pp.threshold,
                origin, radius*od.iloc[0].bend, speed,
                ud[['ts', 'world_x', 'world_z', 'world_yaw', 'yawrate_seconds']].values.T
            )
            
            tc = scipy.stats.pearsonr(yawrate[takeover_i:], od.yawrate_seconds.values[takeover_i:])[0]
            mc = scipy.stats.pearsonr(myawrate[onset_i:], od.yawrate_seconds.values[onset_i:])[0]

            ts = ud.ts.values
            onset = ts[onset_i]
            oyr = od.yawrate_seconds.values
            uyr = ud.yawrate_seconds.values
            try:
                mti = np.flatnonzero(np.abs(myawrate - uyr) > 1e-6)[0] - 1
                fti = np.flatnonzero(np.abs(yawrate - uyr) > 1e-6)[0] - 1
            except IndexError:
                continue
            meancors.append(mc)
            trialcors.append(tc)
            trials.append(dict(
                trial=trial,
                yawrate=yawrate,
                myawrate=myawrate,
                trialcor=tc,
                meancor=mc,
                ud=ud,
                od=od,
                onset_i=onset_i,
                takeover_i=takeover_i,
                mti=mti,
                fti=fti,
                rt=ts[takeover_i] - onset,
                frt=ts[fti] - onset,
                mrt=ts[mti] - onset,
                peakyr=np.max(np.abs(oyr[onset_i:])),
                mpeakyr=np.max(np.abs(myawrate[mti:])),
                fpeakyr=np.max(np.abs(yawrate[fti:])),
                ))
    
    order = np.argsort(meancors)
    n = len(order)
    percentiles = 0.9, 0.5, 0.1
    sample_idx = order[[int(p*n) for p in percentiles]]
    fig, plots = plt.subplots(len(percentiles), sharex=True)
    for plot, p, si in zip(plots, percentiles, sample_idx):
        sd = trials[si]
        plot.set_title(f"perc: {int(p*100)}, trialcor: {sd['trialcor']:2f}, meancor: {sd['meancor']:2f}")
        oi = sd['onset_i']
        ti = sd['takeover_i']
        ts = sd['ud'].ts.values.copy()
        ts -= ts[oi]
        oyr = sd['od'].yawrate_seconds.values
        uyr = sd['ud'].yawrate_seconds.values
        mti = sd['mti']
        fti = sd['fti']
        plot.plot(ts[mti:], sd['myawrate'][mti:], lw=2, color=cmap(0.0))
        plot.plot(ts[fti:], sd['yawrate'][fti:], lw=2, color=cmap(1.0))
        plot.plot(ts[:oi], uyr[:oi], lw=2, color='#003248ff')
        plot.plot(ts[ti:], oyr[ti:], lw=2, color='#003248ff')
        plot.plot(ts[oi:], uyr[oi:], lw=2, color='#ff4000ff')
    plots[-1].set_xlabel('Time from failure (seconds)')
    plots[1].set_ylabel('Turning rate (degrees/second)')
    plt.xlim(-1.0, 7)
    
    plt.figure("preds")
    plt.subplot(1,2,1)
    rts = np.array([(d['rt'], d['mrt'], d['trial']['sab']) for d in trials])
    plt.scatter(rts[:,0], rts[:,1], c=np.abs(rts[:,2]), cmap=cmap, edgecolors='none', s=20, marker='o', alpha=0.8)
    plt.plot([0, 4], [0, 4], lw=2)
    plt.title(f"rho = {scipy.stats.pearsonr(rts[:,0], rts[:,1])[0]}")
    plt.xlabel("Measured reaction time (seconds)")
    plt.ylabel("Predicted reaction time (seconds)")
    plt.gca().set_aspect("equal")
    plt.xlim(0, 4)
    plt.ylim(0, 4)
    
    plt.subplot(1,2,2)
    rts = np.array([(d['peakyr'], d['mpeakyr'], d['trial']['sab']) for d in trials])
    plt.scatter(rts[:,0], rts[:,1], c=np.abs(rts[:,2]), cmap=cmap, edgecolors='none', s=20, marker='o', alpha=0.8)
    plt.plot([5, 25], [5, 25], lw=2)
    plt.title(f"rho = {scipy.stats.pearsonr(rts[:,0], rts[:,1])[0]}")
    plt.xlabel("Measured peak turn rate (degrees/second)")
    plt.ylabel("Predicted peak turn rate (degrees/second)")
    plt.gca().set_aspect("equal")
    plt.xlim(5, 25)
    plt.ylim(5, 25)

    plt.show()

if __name__ == '__main__':
    #takeover_vs_yawrate()
    #loaded_vs_unloaded()
    #plot_projected()
    #plot_waypoint_demo()
    plot_steering_components()
    #sample_trials()
    #plot_failure_scenarios()
