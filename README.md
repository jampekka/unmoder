# Steering automation failure: Unified modeling of detection and recovery

A computational model for how drivers react to automated steering failing. Supplementary material for a poster.

Source code to reproduce the poster's plot included. Run `plot_simulation.py` to reproduce. The called function
in the end of the file must be changed for different plots, and poster plots were created using different reference
durations.

The algorithm is implemented in `model_steering.py`. Current implementation is only for circular paths for
efficiency purposes. The algorithm can handle arbitrary paths.

Raw data used to produce the model fits is unfortunately not yet available.
