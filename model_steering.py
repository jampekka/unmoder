# Copyleft 2019 Jami Pekkanen <pscjjop@leeds.ac.uk>
# Released under AGPLv3, see LICENCE

import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize, minimize_scalar
import itertools
from pprint import pprint


def circle_pursuit_target(preview, time_constant, origin, radius, speed, p_x, p_y, heading):
        d_t = preview
        o_x, o_y = origin
        r = np.abs(radius)
        direction = np.sign(radius)
        v = speed
        prev_t = d_t
        preview_angle = prev_t*v/r
               
        proj_angle = np.arctan2(p_y - o_y, p_x - o_x)
        target_angle = proj_angle - direction*preview_angle
        r_x = r*np.cos(target_angle) + o_x
        r_y = r*np.sin(target_angle) + o_y

        r_x -= p_x
        r_y -= p_y
        t_x = r_x*np.cos(heading) - r_y*np.sin(heading)
        t_y = r_x*np.sin(heading) + r_y*np.cos(heading)

        target_yawrate = 2*speed*t_x/(t_x**2 + t_y**2)
        
        return target_yawrate

class TakeoverCirclePursuiter:
    def __init__(self, preview, time_constant, threshold, origin, radius):
        self.preview = preview
        self.time_constant = time_constant
        self.threshold = threshold
        self.origin = origin
        self.radius = radius

        self.desired_yawrate = 0.0
        self.yawrate = 0.0

    def step(self, dt, speed, p_x, p_y, heading):
        smoothing = 1 - np.exp(-dt/self.time_constant)
        target_yawrate = circle_pursuit_target(
            self.preview, self.time_constant, self.origin, self.radius, speed, p_x, p_y, heading
                )
        self.desired_yawrate += (target_yawrate - self.desired_yawrate)*(smoothing)
        return self.desired_yawrate
    
    def should_take_over(self, yawrate):
        yawrate_error = (yawrate - self.desired_yawrate)
        return np.abs(yawrate_error) > self.threshold

def takeover_circle_pursuiter(preview, time_constant, motor_time_constant, threshold, origin, radius, speed, worldstate, force_takeover_at=np.inf, takeover_latency=0.3):
    ts, p_xs, p_ys, headings, yawrates = worldstate
    maxyr = np.radians(35)
    automatic = True
    prevt = ts[0]
    headings = np.radians(headings)
    yawrates = np.radians(yawrates)
    threshold = np.radians(threshold)
    outyr = np.empty(len(ts))
    out_target_yr = np.empty(len(ts))

    state = p_xs[0], p_ys[0], headings[0], yawrates[0]
    model = TakeoverCirclePursuiter(preview, time_constant, threshold, origin, radius)
    model.desired_yawrate = state[-1]
    
    takeover_at = np.inf

    for i in range(len(ts)):
        t = ts[i]
        dt = t - prevt
        prevt = t
        
        if automatic:
            p_x = p_xs[i]
            p_y = p_ys[i]
            heading = headings[i]
            yawrate = yawrates[i]

        desired_yawrate = model.step(dt, speed, p_x, p_y, heading)
        yawrate_error = (desired_yawrate - yawrate)
        
        motor_smoothing = 1 - np.exp(-dt/(motor_time_constant))

        if automatic:
            eff_yawrate = yawrate
            if not np.isfinite(takeover_at) and model.should_take_over(yawrate):
                takeover_at = t + takeover_latency
            if t >= force_takeover_at or t >= takeover_at:
                automatic = False
                model.desired_yawrate = yawrate # Hack!
        elif not automatic:
            yawrate += yawrate_error*motor_smoothing
        
            eff_yawrate = min(max(yawrate, -maxyr), maxyr)
            heading += eff_yawrate*dt
            p_x += np.sin(heading)*speed*dt
            p_y += np.cos(heading)*speed*dt
        
        outyr[i] = eff_yawrate
        out_target_yr[i] = yawrate_error
    return np.degrees(outyr), np.degrees(out_target_yr)


def fit_trials():
    from reconstruct_trajectory import get_untouched_trajectories, get_track
    data = get_untouched_trajectories()
    fits = []
    takeover_latency=0.3
    for s, sds in itertools.groupby(data.items(), lambda kv: kv[0][0]):
        for g, trial in sds:
            od = trial['otraj']
            ud = trial['traj']
            
            #n = min(len(od), len(ud))

            tmin = max(od.ts.iloc[0], ud.ts.iloc[0])
            tmax = min(od.ts.iloc[-1], ud.ts.iloc[-1])
            ud = ud.query("ts >= @tmin and ts <= @tmax")
            od = od.query("ts >= @tmin and ts <= @tmax")

            try:
                mand = od.iloc[np.flatnonzero(od.autoflag.values)[-1] - 1:]
            except IndexError:
                continue


            tot = mand.ts.iloc[0]
            _, origin = get_track(od.radius.iloc[0], od.bend.iloc[0] < 0)


            radius = 80
            speed = 8

            def predict_yawrate(preview, time_constant):
                pred, error = takeover_circle_pursuiter(
                    preview, time_constant, time_constant, np.inf,
                    origin, radius*od.iloc[0].bend, speed,
                    (ud.ts.values, ud.world_x.values, ud.world_z.values, ud.world_yaw.values, ud.yawrate_seconds.values),
                    tot)
                pred = pred[-len(mand):]

                return pred
            
            def loss(*args):
                pred = predict_yawrate(*args)
                err = pred - mand.yawrate_seconds.values
                # Weigh the error assuming integrated noise
                
                noise_var = (np.arange(len(pred)) + 1)*0.1 + 0.5
                weights = 1.0/np.sqrt(noise_var)
                weights *= len(weights)/np.sum(weights)
                err = (pred - mand.yawrate_seconds.values)*weights
                err = np.mean(np.abs(err)**2)
                return err


            fit = minimize(lambda x: loss(*x), [2.0, 0.3], bounds=[(0.01, None), (0.01, None)])

            
            preview, time_constant = fit.x
            # For the vast majority these seem to fit to almost equal, so let's do away with a parameter
            motor_time_constant = time_constant 
            
            pred, error = takeover_circle_pursuiter(
                    preview, time_constant, motor_time_constant, np.inf,
                     origin, radius*od.iloc[0].bend, speed,
                    (ud.ts.values, ud.world_x.values, ud.world_z.values, ud.world_yaw.values, ud.yawrate_seconds.values))
            threshold = np.abs(error[ud.ts.values.searchsorted(tot - takeover_latency) - 1])
            fit = dict(
                preview=preview,
                time_constant=time_constant,
                threshold=threshold,
                takeover_latency=takeover_latency,
                loss=fit.fun,
                duration=ud.ts.iloc[-1],
                **{k: v for k, v in trial.items() if k not in ['traj', 'otraj']}
            ) 
            fits.append(fit)
        
    fits = pd.DataFrame.from_records(fits)
    fits.to_csv("steering_fits.csv", index=False)
            

if __name__ == '__main__':
    fit_trials()
